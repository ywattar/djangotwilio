import thread
import time
from blog.models import DeliveryStatus
from django.http import HttpResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt
from threading import Thread
from time import sleep
from django_twilio.decorators import twilio_view
from push_notifications.admin import DeviceAdmin
from rest_framework import status
from rest_framework.views import APIView
from twilio import twiml
from push_notifications.models import APNSDevice, GCMDevice
from twilio.twiml import Response
from rest_framework.restresponse import RestResponse


#device = GCMDevice.objects.get(registration_id='eLO4byN9NXU:APA91bGhB5imqa5xPX3Nj-iGSLqbAHgVD9RQYGehjHXt68wphY-Jsm_oJyk9OGJwsO39zhieN7Cr-iuRxKja8IVbt4wwQ-193ZchLCQ2ICTF1h7K59vFBf_B5QtkHapdDD4_RM_kU4O0')
device = GCMDevice.objects.get(registration_id='m0XCSLRGzmo:APA91bEqZclYXw5JSZXay287wdvb0FsL85C-4kM_7ODvBI-c5O2OmTlKOjF1TzSpJRzKo0YJbmYnmvXTQWXYzVH6vZSFFJKxFoe7UU-zfMc2V6xkSENASbe4CyW8rlv3EMroI-ExMbjO')
@twilio_view
def gather_digits(request):

    twilio_response = Response()

    with twilio_response.gather(action='/respond/', numDigits=1) as g:
        g.say('Press one to Notify Yaser, or  press two to notify Nazih')
        g.pause(length=1)
        g.say('Press one to notify  Yaser, or press two to notify Nazih')

    return twilio_response

@twilio_view
def handle_response(request):
    message_id_numbers=len(DeliveryStatus.objects.all())


    if  message_id_numbers!=0:
        all_message_id=[p.MessageId for p in DeliveryStatus.objects.all()]
        twilio_message_id=all_message_id[len(all_message_id)-1]+1
        print("twilio_message_id", twilio_message_id)

    else:
        twilio_message_id=11
        print ("twilio_message_id 0",twilio_message_id)

    digits = request.POST.get('Digits', '')
    twilio_response = Response()



    if digits == '1':
        twilio_response.say('A text message is on its way')
        device.send_message("you got paged! "+str(twilio_message_id))
        DeliveryStatus.objects.create(MessageId=twilio_message_id,Delivered=False)
        print("delivery_status2 ", DeliveryStatus.objects.get(MessageId=twilio_message_id))
        print("delivery status 2 ",DeliveryStatus.objects.get(MessageId=twilio_message_id).Delivered)
        twilio_response.say('Your message has been sent. Thank you')


    if digits == '2':
        twilio_response.say('A text message is on its way')
        twilio_response.sms('You got paged!',to='2266068196')

    return twilio_response


class MessageDelivered (APIView):
    def post(self,request,format=None):

        #MessageDelivered.msgId+=1
        sent_message_id=[p.MessageId for p in DeliveryStatus.objects.all()]
        print ("SENT MSGID", sent_message_id)
        print("Delivery Status Databases:(Before Update) ",DeliveryStatus.objects.all())
        print (request.POST)
        myDict=qdict_to_dict(request.POST)
        print("MYDIC", myDict)
        for id in sent_message_id:
            idStr=str(id)+"}"
            if myDict['{msgId'] == idStr:
                print('DELIVERED',idStr)

                #DeviceAdmin.msgIdArray.remove(id)
                deliverystatus1=DeliveryStatus.objects.get(MessageId=id)
                deliverystatus1.Delivered=True
                deliverystatus1.save()
            #
            # else:
            #     if not MessageDelivered.notDeliveredMsgId.__contains__(id):
            #             MessageDelivered.notDeliveredMsgId.append(id)
            #             print('NOT DELIVERED', idStr)
        #resendNotDeliveredMsgTimer(MessageDelivered.notDeliveredMsgId)
        notDeliveredMsgId=[n.MessageId for n in DeliveryStatus.objects.filter(Delivered=False)]
        print("NOT DELIVERED MSGID: ",notDeliveredMsgId )
        print("Delivery Status Databases:(After Update) ",DeliveryStatus.objects.all())
        return RestResponse("Delivered",status=status.HTTP_200_OK)

def qdict_to_dict(qdict):
    """Convert a Django QueryDict to a Python dict."""
    return {k: v[0] if len(v) == 1 else v for k, v in qdict.lists()}

# def resendNotDeliveredMsgTimer (notDeliveredMsgIdArry):
#     for id1 in notDeliveredMsgIdArry:
#         time.sleep(0.1)
#         device.send_message("resend notification"+str(id1))
#         break
