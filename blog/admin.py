from django.contrib import admin
from .models import SignUp, DeliveryStatus
from .form import SignUpForm
# Register your models here.

class SignUpAdmin(admin.ModelAdmin):
    list_display = ["__unicode__","full_name","phone_number","timestamp"]
    form = SignUpForm

class DeliveryStatusAdmin(admin.ModelAdmin):
    list_display = ["__unicode__","DeliveryTime"]
admin.site.register(SignUp,SignUpAdmin)
admin.site.register(DeliveryStatus,DeliveryStatusAdmin)

