# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-01 05:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_deliverystatus'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliverystatus',
            name='TimeStamp',
        ),
    ]
